//Create Gold DB
Create or Replace Database DB_PRD_SMB;
Create or Replace Schema Gold with managed access;
Create or Replace Schema Reporting;
Create or Replace TRANSIENT Schema Landing DATA_RETENTION_TIME_IN_DAYS = 1;
Drop Schema Public;